const { google } = require('googleapis');
const { OAuth2 } = google.auth;
//oAuth2('client_id', 'client_secret')
const oAuth2Client = new OAuth2(
    '1077737725002-mqp2tq2v9rjadnneqo7oiiavpgasp44p.apps.googleusercontent.com',
    'nVOg9xoAtEqAoVQdgRLxZOYR'
    );

oAuth2Client.setCredentials({
    refresh_token: '1//04YeF2KoVizjyCgYIARAAGAQSNwF-L9IroGLg8DUwHJrC_erdxWuR9qWDL27afqNCusYRY0Y1GV3KeYI78l4I_rXxexRKmlRAW88'
});

const calendar = google.calendar({version: 'v3', auth: oAuth2Client});

const eventStartTime = new Date();
eventStartTime.setDate(eventStartTime.getDay()+2);

const eventEndTime = new Date();
eventEndTime.setDate(eventEndTime.getDay()+2);
eventEndTime.setMinutes(eventEndTime.getMinutes()+45);

/*
calendar.calendarList.list({
    'maxResults': 10
}).then( res => {
    const cal = res.data.items;
    console.log('CALENDARS: ', cal)
})
*/


calendar.events.list({
    'calendarId': 'l2vphoc80mb197tet84hh0mi70@group.calendar.google.com',
    'timeMin': (new Date()).toISOString(),
    'showDeleted': false,
    'singleEvents': true,
    'maxResults': 10,
    'orderBy': 'startTime'
}).then(response => {
    const events = response.data.items;
    console.log('EVENTS: ', events);
})

/*
const event = {
    summary: 'Teste Evento GCTI',
    location: 'Terminal GACIV', 
    description: 'Feriado da Independência do Brasil',
    start: {
        dateTime: eventStartTime,
        timeZone: 'America/Denver'
    },
    end: {
        dateTime: eventEndTime,
        timeZone: 'America/Denver'
    },
    colorId: 1,
}

calendar.freebusy.query({
    resource: {
        timeMin: eventStartTime,
        timeMax: eventEndTime,
        timeZone: 'America/Denver',
        items: [{id: 'primary'}]
    }
}, (err, res) => {
    if(err) return console.log('Free Busy Query Error: ' + err);

    const eventsArr = res.data.calendars.primary.busy;
    if(eventsArr.length === 0) return calendar.events
    .insert({calendarId: 'primary', resource: event}, 
    err => { 
        if (err) return console.error('Calendar Event Creation Error: ', err);
        
        return console.log('Calendar Event Created.')
    }) 
    return console.log('Fail...')
})
*/